# Simple Photo Gallery

## Features
- Thumbnail bar that can be toggled on and off
- Autoplay functionality

## Demo
https://js271.gitlab.io/photo-gallery-app/
