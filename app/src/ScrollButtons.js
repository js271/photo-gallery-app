import React from 'react';

import scrollLeftImg from "./img-assets/scroll_left_img_32w.png"
import scrollRightImg from "./img-assets/scroll_right_img_32w.png"
import playImg from "./img-assets/play_img_32w.png"
import pauseImg from "./img-assets/pause_img_32w.png"

export class ScrollButtons extends React.Component {
  render() {
    return (
      <React.Fragment>
        <button
          className="scroll-button centered-bg-img"
          onClick={this.props.scrollLeft}
          style={{backgroundImage: `url(${scrollLeftImg})`}}>
        </button>
        <button
          className={"scroll-button centered-bg-img"}
          onClick={this.props.toggleAutoplay}
          style={{backgroundImage: this.props.autoplayEnabled ? `url(${pauseImg})` : `url(${playImg})`}}>
        </button>
        <button
          className="scroll-button centered-bg-img"
          onClick={this.props.scrollRight}
          style={{backgroundImage: `url(${scrollRightImg})`}}>
        </button>
      </React.Fragment>
    )
  }
}
