import React from 'react';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';

import { Thumbnail } from './Thumbnail.js';
import { ScrollButtons } from './ScrollButtons.js';

import fullscreenEnter from "./img-assets/fullscreen-enter.png"

export class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { thumbnailBarVisible: true };
    this.scrollLeft = this.scrollLeft.bind(this);
    this.scrollRight = this.scrollRight.bind(this);
    this.selectImg = this.selectImg.bind(this);
    this.scrollIntoView = this.scrollIntoView.bind(this);
    this.toggleThumbnailBar = this.toggleThumbnailBar.bind(this);
    this.osComponentRef = React.createRef();
  }
  scrollLeft() {
    this.props.changeImgRel(-1);
  }
  scrollRight() {
    this.props.changeImgRel(1);
  }
  selectImg(index) {
    this.props.changeImg(index);
  }
  scrollIntoView(thumbnail) {
    if (this.osComponentRef) {
      let instance = this.osComponentRef.current.osInstance();
      if (instance) {
        instance.scroll({ el : thumbnail.ref.current, scroll : { x : "ifneeded", y : "never" } }, 100);
      }
    }
  }
  toggleThumbnailBar() {
    this.setState({ thumbnailBarVisible: !this.state.thumbnailBarVisible });
  }

  render() {
    let thumbnailBar = [];
    for (let i = 0; i < this.props.appState.numImages; i++) {
      thumbnailBar.push(
        <React.Fragment key={"thumbnail_" + i}>
          <span className="thumbnail-helper"></span>
          <Thumbnail
            imgIndex={i}
            selectImg={this.selectImg}
            scrollIntoView={this.scrollIntoView}
            active={i === this.props.appState.activeImgIndex}
            pictureInfo={this.props.appState.pictureInfo}
          />
        </React.Fragment>
      );
    }
    return (
      <div id="navigation">
        <div id="scroll-buttons">
          <button
            id="fullscreen-button"
            className={"scroll-button centered-bg-img"}
            onClick={this.toggleThumbnailBar}
            style={{backgroundImage: `url(${fullscreenEnter})`}}>
          </button>
          <ScrollButtons
            scrollLeft={this.scrollLeft}
            toggleAutoplay={this.props.toggleAutoplay}
            scrollRight={this.scrollRight}
            autoplayEnabled={this.props.autoplayEnabled}
          />
          <span id="picture-index">{this.props.appState.activeImgIndex + 1}/{this.props.appState.numImages}</span>
        </div>
        <OverlayScrollbarsComponent
          options={{overflowBehavior: { x: "scroll" }, className: "os-theme-light"}}
          className={!this.state.thumbnailBarVisible && " hidden"}
          id="thumbnail-bar"
          ref={this.osComponentRef}>
          {thumbnailBar}
        </OverlayScrollbarsComponent>
      </div>
    )
  }
}
