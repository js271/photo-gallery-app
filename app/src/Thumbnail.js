import React from 'react';

export class Thumbnail extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.ref = React.createRef();
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.active && this.props.active) {
      this.props.scrollIntoView(this);
    }
  }
  handleClick() {
    this.props.selectImg(this.props.imgIndex);
  }
  render() {
    const pictureInfo = this.props.pictureInfo;

    let imgSrc = "";
    if (pictureInfo["pictures"][this.props.imgIndex]) {
      let filename = pictureInfo["pictures"][this.props.imgIndex]["url"];
      imgSrc = `${process.env.PUBLIC_URL}/gallery-images/big/${filename}`;
    }
    else {
      imgSrc = `${process.env.PUBLIC_URL}/not_found.jpg`;
    }

    return (
      <div
        className={"thumbnail" + (this.props.active ? " thumbnail--active" : "")}
        ref={this.ref}>
        <img
          onClick={this.handleClick}
          src={imgSrc}
          alt=""
          draggable="false"
        />
      </div>
    )
  }
}
