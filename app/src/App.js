import React from 'react';
import 'what-input';

import { Display } from './Display.js';
import { Navigation } from './Navigation.js';

import './reset.css';
import './App.css';

import pictureInfo from "./pictureInfo.json";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImgIndex: 0,
      autoplayEnabled: false,
      autoplayInterval: null,
      numImages: pictureInfo["pictures"].length,
      pictureInfo: pictureInfo
    };
    this.toggleAutoplay = this.toggleAutoplay.bind(this);
    this.changeImg = this.changeImg.bind(this);
    this.changeImgRel = this.changeImgRel.bind(this);
  }
  toggleAutoplay() {
    if (!this.state.autoplayEnabled) {
      let interval = setInterval(this.changeImgRel.bind(null, 1), 3000);
      this.setState({ autoplayInterval: interval });
    }
    else {
      clearInterval(this.state.autoplayInterval);
    }
    this.setState({ autoplayEnabled: !this.state.autoplayEnabled });
  }
  changeImg(id) {
    if (this.state.autoplayEnabled) { // reset interval if autoplay is on
      clearInterval(this.state.autoplayInterval);
      let interval = setInterval(this.changeImgRel.bind(null, 1), 3000);
      this.setState({ autoplayInterval: interval });
    }
    if (id >= 0 && id < this.state.numImages) {
      this.setState({ activeImgIndex: id });
    }
  }
  changeImgRel(offset) {
    const newImgIndex = (this.state.activeImgIndex + this.state.numImages + offset) % this.state.numImages;
    this.changeImg(newImgIndex);
  }
  render() {
    return (
      <React.Fragment>
        <Display
          appState={this.state}
        />
        <Navigation
          changeImg={this.changeImg}
          changeImgRel={this.changeImgRel}
          toggleAutoplay={this.toggleAutoplay}
          appState={this.state}
        />
      </React.Fragment>
    );
  }
}

export default App;
