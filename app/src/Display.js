import React from 'react';

export class Display extends React.Component {
  render() {
    let images = [];
    const pictureInfo = this.props.appState.pictureInfo;

    for (let i = 0; i < this.props.appState.numImages; i++) {
      let imgSrc = "";
      let classes = "display-img centered-bg-img";

      if (pictureInfo["pictures"][i]) {
        let filename = pictureInfo["pictures"][i]["url"];
        imgSrc = `${process.env.PUBLIC_URL}/gallery-images/big/${filename}`;
      }
      else {
        imgSrc = `${process.env.PUBLIC_URL}/not_found.jpg`;
      }
      if (i === this.props.appState.activeImgIndex) {
        classes += " display-img-visible";
      }

      images.push(
        <img
          key={"display-img_" + i}
          src={`${imgSrc}`}
          alt=""
          className={classes}
        />
      );
    }
    return (
      <div id="display">
        {images}
      </div>
    );
  }
}
